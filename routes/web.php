<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('');

Route::get('/principal', 'HomeController@index')->name('principal');

Route::post('/send_mail', 'HomeController@send_mail')->name('send_mail');

Auth::routes();

//rutas de usuario
Route::get('/admin', 'HomeController@admin')->name('admin');

Route::get('/nuevo_usuario', 'HomeController@nuevo_usuario')->name('nuevo_usuario');

Route::get('/ver_usuario', 'HomeController@ver_usuario')->name('ver_usuario');

Route::get('/delete/{id}', 'HomeController@destroy')->name('delete');

Route::get('/editar/{id}', 'HomeController@editar')->name('editar');

Route::get('/habilitar/{id}', 'HomeController@habilitar')->name('habilitar');

Route::get('/deshabilitar/{id}', 'HomeController@deshabilitar')->name('deshabilitar');

Route::post('/edit', 'HomeController@edit')->name('edit');

Route::post('/get_mail', 'HomeController@get_mail')->name('get_mail');

//rutas de evento
Route::get('/ver_evento', 'HomeController@ver_evento')->name('ver_evento');

Route::get('/delete_event/{id}', 'HomeController@destroy_event')->name('delete_event');

Route::get('/habilitar_event/{id}', 'HomeController@habilitar_event')->name('habilitar_event');

Route::get('/deshabilitar_event/{id}', 'HomeController@deshabilitar_event')->name('deshabilitar_event');

Route::get('/nuevo_evento', 'HomeController@nuevo_evento')->name('nuevo_evento');

Route::post('/guardar_evento', 'HomeController@guardar_evento')->name('guardar_evento');

Route::get('/editar_evento/{id}', 'HomeController@editar_evento')->name('editar_evento');

Route::post('/edit_evento', 'HomeController@edit_evento')->name('edit_evento');
