<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->id();
            $table->integer('id_usuario');
            $table->string('titulo');
            $table->string('imagen');
            $table->timestamp('fecha_evento');
            $table->string('lugar')->nullable();
            $table->string('director')->nullable();
            $table->integer('estado');
            $table->string('link')->nullable();
            $table->string('porc_descuento')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
