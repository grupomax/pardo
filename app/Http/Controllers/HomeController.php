<?php

namespace App\Http\Controllers;

use App\Evento;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','send_mail']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $eventos = Evento::where('estado', 1)->orderBy('id', 'DESC')->get();
        return view('pages/principal')->with(['eventos' => $eventos]);
    }


    public function send_mail(request $request){
        $details = [
            'nombre' => $request->nombre,
            'telefono' => $request->telefono,
            'email' => $request->email,
            'mensaje' => $request->mensaje
        ];
        \Mail::to('pardoproducciones.arg@gmail.com')->send(new \App\Mail\Mail($details));

    }


    public function admin()
    {
        return view('admin/panel');
    }

    //funciones de usarios

    public function nuevo_usuario()
    {
        $users = Auth::user();
        if ($users->user_type==1){
            return view('admin/nuevo_admin');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function ver_usuario()
    {
        $user = Auth::user();
        if ($user->user_type==1){
            $users = User::paginate(20);
            return view('admin/ver_admin')->with(['users' => $users]);
        }
        else{
            return view('errors/denegado');
        }
    }

    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->user_type==1){
            $usuario = User::findOrFail($id);
            $usuario->delete();
            return redirect('ver_usuario')->with('repuesta', 'Usuario eliminado exitosamente.');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function habilitar($id)
    {
        $user = Auth::user();
        if ($user->user_type==1){
            $usuario = User::findOrFail($id);
            $usuario->status = 1;
            $usuario->save();
            return redirect('ver_usuario')->with('repuesta', 'Usuario habilitado exitosamente.');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function deshabilitar($id)
    {
        $user = Auth::user();
        if ($user->user_type==1){
            $usuario = User::findOrFail($id);
            $usuario->status = 0;
            $usuario->save();
            return redirect('ver_usuario')->with('repuesta', 'Usuario deshabilitado exitosamente.');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function editar($id){
        $user = Auth::user();
        if ($user->user_type==1){
            $usuario = User::findOrFail($id);
            return view('admin/editar_admin')->with(['usuario' => $usuario]);
        }
        else{
            return view('errors/denegado');
        }
    }

    public function edit(request $request){
        $user = Auth::user();
        if ($user->user_type==1){
            $usuario = User::findOrFail($request->id_user);
            $usuario->name = $request->name;
            $usuario->email = $request->email;
            $usuario->user_type = $request->user_type;
            if ($request->password!=null) {
                $usuario->password = Hash::make($request->password);
            }
            $usuario->save();
            return redirect('ver_usuario')->with('repuesta', 'Usuario editado exitosamente.');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function get_mail(request $request)
    {
        $user = Auth::user();
        if ($user->user_type==1){
            $id = $request->id;
            $email = $request->email;
            if (User::where('id', '!=' , $id)->where('email', $email)->exists()){
                return response()->json(['status'=>true]);
            }else{
                return response()->json(['status'=>false]);
            }
        }
        else{
            return view('errors/denegado');
        }
    }

    //funciones de evento

    public function ver_evento()
    {
        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){

            $eventos = Evento::select([
                'eventos.id',
                'eventos.titulo',
                'eventos.imagen',
                'eventos.fecha_evento',
                'eventos.lugar',
                'eventos.estado',
                'eventos.director',
                'eventos.porc_descuento',
                'usuario.name',
            ])->join('users as usuario','usuario.id','=','eventos.id_usuario')->orderBy('id', 'DESC')->paginate(15);

            return view('admin/ver_evento')->with(['eventos' => $eventos]);
        }
        else{
            return view('errors/denegado');
        }
    }


    public function destroy_event($id)
    {
        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){
            $event = Evento::findOrFail($id);
            $path = public_path('image\\publicados\\').$event->imagen;
            //borramos la imagen
            unlink($path);
            $event->delete();
            return redirect('ver_evento')->with('repuesta', 'Evento eliminado exitosamente.');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function habilitar_event($id)
    {
        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){
            $event = Evento::findOrFail($id);
            $event->estado = 1;
            $event->save();
            return redirect('ver_evento')->with('repuesta', 'Evento activado exitosamente.');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function deshabilitar_event($id)
    {
        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){
            $event = Evento::findOrFail($id);
            $event->estado = 0;
            $event->save();
            return redirect('ver_evento')->with('repuesta', 'Evento dado de baja exitosamente.');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function nuevo_evento()
    {
        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){
            return view('admin/nuevo_evento');
        }
        else{
            return view('errors/denegado');
        }
    }

    public function guardar_evento(request $request){
        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){

            $file = $request->file('imagen');
            //obtenemos el nombre del archivo
            $nombre_file =  time()."_".$file->getClientOriginalName();
            //indicamos que queremos guardar un nuevo archivo en el disco local
            Storage::disk('imagen_evento')->put($nombre_file, file_get_contents($file));

            $evento = New Evento();
            $evento->id_usuario = $user->id;
            $evento->titulo= mb_strtoupper($request->titulo, 'UTF-8');
            $evento->imagen= $nombre_file;
            $evento->fecha_evento= $request->fecha_evento;
            $evento->lugar= ucwords($request->lugar);
            $evento->director= ucwords($request->director);
            $evento->estado= 1;
            $evento->link= $request->link;
            $evento->save();

            return redirect('ver_evento')->with('repuesta', 'Evento registrado exitosamente.');

        }
        else{
            return view('errors/denegado');
        }
    }

    public function editar_evento($id){

        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){

            $evento = Evento::findOrFail($id);

            return view('admin/editar_evento')->with(['evento' => $evento]);
        }
        else{
            return view('errors/denegado');
        }
    }

    public function edit_evento(request $request){

        $user = Auth::user();
        if ($user->user_type==1 || $user->user_type==2){

        //obtenemos el nombre del archivo
            $evento = Evento::findOrFail($request->id_evento);
            $evento->titulo= mb_strtoupper($request->titulo, 'UTF-8');

            if ($request->imagen) {
                //borramos la vieja imagen
                $path = public_path('image\\publicados\\').$request->old_imagen;
                unlink($path);
                //guardamos la nueva imagen
                $file = $request->file('imagen');
                $nombre_file = time() . "_" . $file->getClientOriginalName();
                Storage::disk('imagen_evento')->put($nombre_file, file_get_contents($file));
                $evento->imagen = $nombre_file;
            } else {
                $evento->imagen = $request->old_imagen;
            }
            $evento->fecha_evento= $request->fecha_evento;
            $evento->lugar= ucwords($request->lugar);
            $evento->director= ucwords($request->director);
            $evento->link= $request->link;
            $evento->save();

            return redirect('ver_evento')->with('repuesta', 'Evento editado exitosamente.');

        }
        else{
            return view('errors/denegado');
        }
    }

}
