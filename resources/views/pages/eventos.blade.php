<section id="eventos" class="seccion_line_eventos section elementor-element elementor-element-b971b1b elementor-section-full_width qodef-elementor-content-grid qodef-in-focus elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="b971b1b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-container elementor-column-gap-no">
        <h3 class="titulos_principales">Próximos eventos</h3>
    </div>
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-b4c5343 elementor-column elementor-col-100 elementor-top-column" data-id="b4c5343" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-9861592 elementor-widget elementor-widget-pelicula_core_blog_list" data-id="9861592" data-element_type="widget" data-widget_type="pelicula_core_blog_list.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-blog qodef--list qodef-skin--light qodef-item-layout--standard  qodef-grid qodef-layout--columns   qodef-gutter--normal qodef-col-num--3 qodef-item-layout--standard qodef--no-bottom-space qodef-pagination--off qodef-responsive--custom qodef-col-num--1440--3 qodef-col-num--1366--3 qodef-col-num--1024--2 qodef-col-num--768--2 qodef-col-num--680--1 qodef-col-num--480--1" data-options="{&quot;plugin&quot;:&quot;pelicula_core&quot;,&quot;module&quot;:&quot;blog\/shortcodes&quot;,&quot;shortcode&quot;:&quot;blog-list&quot;,&quot;post_type&quot;:&quot;post&quot;,&quot;next_page&quot;:&quot;2&quot;,&quot;max_pages_num&quot;:2,&quot;skin&quot;:&quot;light&quot;,&quot;behavior&quot;:&quot;columns&quot;,&quot;custom_proportions&quot;:&quot;no&quot;,&quot;images_proportion&quot;:&quot;full&quot;,&quot;columns&quot;:&quot;3&quot;,&quot;columns_responsive&quot;:&quot;custom&quot;,&quot;columns_1440&quot;:&quot;3&quot;,&quot;columns_1366&quot;:&quot;3&quot;,&quot;columns_1024&quot;:&quot;2&quot;,&quot;columns_768&quot;:&quot;2&quot;,&quot;columns_680&quot;:&quot;1&quot;,&quot;columns_480&quot;:&quot;1&quot;,&quot;space&quot;:&quot;normal&quot;,&quot;posts_per_page&quot;:&quot;3&quot;,&quot;orderby&quot;:&quot;date&quot;,&quot;order&quot;:&quot;DESC&quot;,&quot;additional_params&quot;:&quot;tax&quot;,&quot;tax&quot;:&quot;category&quot;,&quot;tax_slug&quot;:&quot;camera&quot;,&quot;layout&quot;:&quot;standard&quot;,&quot;title_tag&quot;:&quot;h4&quot;,&quot;title_custom_style&quot;:&quot;no&quot;,&quot;content_padding_bottom&quot;:&quot;20px&quot;,&quot;pagination_type&quot;:&quot;no-pagination&quot;,&quot;enable_comments&quot;:&quot;no&quot;,&quot;enable_social_share&quot;:&quot;yes&quot;,&quot;object_class_name&quot;:&quot;PeliculaCoreBlogListShortcode&quot;,&quot;taxonomy_filter&quot;:&quot;category&quot;,&quot;additional_query_args&quot;:{&quot;tax_query&quot;:[{&quot;taxonomy&quot;:&quot;category&quot;,&quot;field&quot;:&quot;slug&quot;,&quot;terms&quot;:&quot;camera&quot;}]},&quot;content_styles&quot;:[&quot;padding-bottom: 20px&quot;],&quot;space_value&quot;:15}">
                                    <div class="qodef-grid-inner clear">
                                        <div class="owl-carousel owl-theme" id="eventos_seccion">
                                            @foreach($eventos as $evento)
                                            <div class="item">
                                                <article class="qodef-e qodef-grid-item qodef-item--full qodef-e-holder-main qodef-has-post-media post-942 post type-post status-publish format-standard has-post-thumbnail hentry category-camera tag-drama tag-film tag-review tag-video">
                                            <div class="qodef-e-inner">
                                                <div class="qodef-e-media">
                                                    <div class="qodef-e-media-image">
                                                        <a itemprop="url" @if($evento->link!=null) href="{{$evento->link}}" target="_blank" @else @endif>
                                                            <img width="100%" height="410" src="{{ asset('image/publicados/'.$evento->imagen) }}" class="attachment-full size-full wp-post-image" alt="a" srcset="{{ asset('image/publicados/'.$evento->imagen) }}">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="qodef-e-content qodef-e-content-main qodef-e-content--appear" style="padding-bottom: 20px; margin-top: -17px;">
                                                    <div class="qodef-e-text">
                                                        <h3 itemprop="name" class="qodef-e-title qodef-e-title-main entry-title">
                                                            <a itemprop="url" class="qodef-e-title-link" href="#">{{$evento->titulo}}</a>
                                                        </h3>
                                                        <div class="qodef-e-info qodef-info--top">
                                                            <div itemprop="dateCreated" class="qodef-e-info-item qodef-e-info-date entry-date published updated">
                                                                <a itemprop="url">{{ \Carbon\Carbon::parse($evento->fecha_evento)->format('d/m/Y') }}</a>
                                                                <meta itemprop="interactionCount" content="UserComments: 2">
                                                            </div><div class="qodef-e-info-item qodef-e-info-category">
                                                                <a rel="category tag">{{$evento->lugar}}</a>
                                                            </div>
                                                            <div class="qodef-e-info-item qodef-e-info-author">
                                                                <span class="qodef-e-info-author-label"></span>
                                                                <a itemprop="author" class="qodef-e-info-author-link">{{$evento->director}}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
