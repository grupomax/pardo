    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-weight: 200;
            margin: 0;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
        #qodef-page-outer {
            margin-top: 0 !important;
            width: 100%;
        }
        .contenedor_1{
            position: absolute;
            z-index: 25;
            margin-top: -32%;
            padding-left: 5%;
        }
        p.texto_slide{
            color: white;
            font-family: "Playfair Display";
            font-weight: bold;
            font-size: 72px;
            letter-spacing: 2px;
            line-height: 1.4;
        }
        .contenedor_redes{
            position: absolute;
            z-index: 25;
            margin-top: -35%;
            right: -40px;
        }
        .icono_redes{
            width: 28% !important;
            height: auto !important;
            margin: 15px 0px;
        }
        .qodef-opener-icon.qodef-source--predefined:hover{
            color: white;
        }
        .video_prin{
            height: auto;
            min-width: 100%;
            width: 100%;
            margin: 0;
        }
    </style>
@include('layouts.header')
<!--pc-->
<section id="inicio" class="seccion_line_teatros position-ref full-height">
    <div id="qodef-page-outer">
        <div id="qodef-page-inner" class="qodef-content-full-width">
            <main id="qodef-page-content" class="qodef-grid qodef-layout--template ">
                <div class="qodef-grid-inner">
                    <div class="qodef-grid-item qodef-page-content-section qodef-col--12">
                        <div data-elementor-type="wp-page" data-elementor-id="258" class="elementor elementor-258" data-elementor-settings="[]">
                            <div class="elementor-inner">
                                <div class="elementor-section-wrap">
                                    <section class="elementor-element elementor-element-3dea8a9 elementor-section-full_width elementor-section-height-default elementor-section-height-default qodef-elementor-content-no elementor-section elementor-top-section" data-id="3dea8a9" data-element_type="section">
                                        <div class="elementor-container elementor-column-gap-no">
                                            <div class="elementor-row">
                                                <div class="elementor-element elementor-element-b95c7ac elementor-column elementor-col-100 elementor-top-column" data-id="b95c7ac" data-element_type="column">
                                                    <div class="elementor-column-wrap  elementor-element-populated">
                                                        <div class="elementor-widget-wrap">
                                                            <div class="elementor-element elementor-element-51960d1 elementor-widget elementor-widget-pelicula_core_portfolio_list" data-id="51960d1" data-element_type="widget" data-widget_type="pelicula_core_portfolio_list.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="qodef-shortcode qodef-m qodef-in-focus qodef-portfolio-list qodef-item-layout--info-on-hover qodef-slider-layout--predefined2 qodef-grid qodef-swiper-container qodef-gutter--no qodef-col-num--3 qodef--no-bottom-space qodef-pagination--off qodef-responsive--predefined qodef-hover-animation--fade-in swiper-container-initialized swiper-container-horizontal qodef-swiper--initialized" data-options="{&quot;slidesPerView&quot;:1.899999999999999911182158029987476766109466552734375,&quot;spaceBetween&quot;:0,&quot;loop&quot;:true,&quot;autoplay&quot;:true,&quot;speed&quot;:&quot;&quot;,&quot;speedAnimation&quot;:&quot;&quot;,&quot;centeredSlides&quot;:true,&quot;slideToClickedItem&quot;:true}">
                                                                        <div class="owl-carousel owl-theme" id="cabecera">
                                                                            <div class="item">
                                                                                <video class="video_prin" src="{{ asset('image/video/video_principal.mp4') }}" muted preload playsinline></video>
                                                                            {{--<div class="contenedor_1"><p class="texto_slide">LOS MEJORES EVENTOS <br>LOS VIVIS CON NOSOTROS</p></div>--}}
                                                                                <div class="contenedor_redes">
                                                                                    <hr>
                                                                                    <a href="https://www.instagram.com/pardoproductora/" target="_blank"><img class="icono_redes" src="{{ asset('image/iconos/instagram-03.svg') }}"></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div><!-- close #qodef-page-inner div from header.php -->
    </div><!-- close #qodef-page-outer div from header.php -->
</section>
<!--cel-->
    @include('pages.teatros')

{{--    @include('pages.video')--}}
    @include('pages.eventos')
    @include('pages.nosotros')

{{--    @include('pages.mapa')--}}

    @include('layouts.footer')
    <script>
        if ($(window).width() <= 767) {
            $('.contenedor_redes').css('display','none');
            $('.video_prin').attr("src" , `{{ asset('image/video/video_principal_cel.mp4') }}`);
        }
        $( document ).ready(function() {
            $('.video_prin')[0].play();
            $('.video_prin').play;
        });
    </script>

