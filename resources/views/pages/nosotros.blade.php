<section id="nosotros" class="section elementor-element elementor-element-b971b1b elementor-section-full_width qodef-elementor-content-grid qodef-in-focus elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="b971b1b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-b4c5343 elementor-column elementor-col-100 elementor-top-column" data-id="b4c5343" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-9861592 elementor-widget elementor-widget-pelicula_core_blog_list" data-id="9861592" data-element_type="widget" data-widget_type="pelicula_core_blog_list.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-blog qodef--list qodef-skin--light qodef-item-layout--standard  qodef-grid qodef-layout--columns   qodef-gutter--normal qodef-col-num--3 qodef-item-layout--standard qodef--no-bottom-space qodef-pagination--off qodef-responsive--custom qodef-col-num--1440--3 qodef-col-num--1366--3 qodef-col-num--1024--2 qodef-col-num--768--2 qodef-col-num--680--1 qodef-col-num--480--1" data-options="{&quot;plugin&quot;:&quot;pelicula_core&quot;,&quot;module&quot;:&quot;blog\/shortcodes&quot;,&quot;shortcode&quot;:&quot;blog-list&quot;,&quot;post_type&quot;:&quot;post&quot;,&quot;next_page&quot;:&quot;2&quot;,&quot;max_pages_num&quot;:2,&quot;skin&quot;:&quot;light&quot;,&quot;behavior&quot;:&quot;columns&quot;,&quot;custom_proportions&quot;:&quot;no&quot;,&quot;images_proportion&quot;:&quot;full&quot;,&quot;columns&quot;:&quot;3&quot;,&quot;columns_responsive&quot;:&quot;custom&quot;,&quot;columns_1440&quot;:&quot;3&quot;,&quot;columns_1366&quot;:&quot;3&quot;,&quot;columns_1024&quot;:&quot;2&quot;,&quot;columns_768&quot;:&quot;2&quot;,&quot;columns_680&quot;:&quot;1&quot;,&quot;columns_480&quot;:&quot;1&quot;,&quot;space&quot;:&quot;normal&quot;,&quot;posts_per_page&quot;:&quot;3&quot;,&quot;orderby&quot;:&quot;date&quot;,&quot;order&quot;:&quot;DESC&quot;,&quot;additional_params&quot;:&quot;tax&quot;,&quot;tax&quot;:&quot;category&quot;,&quot;tax_slug&quot;:&quot;camera&quot;,&quot;layout&quot;:&quot;standard&quot;,&quot;title_tag&quot;:&quot;h4&quot;,&quot;title_custom_style&quot;:&quot;no&quot;,&quot;content_padding_bottom&quot;:&quot;20px&quot;,&quot;pagination_type&quot;:&quot;no-pagination&quot;,&quot;enable_comments&quot;:&quot;no&quot;,&quot;enable_social_share&quot;:&quot;yes&quot;,&quot;object_class_name&quot;:&quot;PeliculaCoreBlogListShortcode&quot;,&quot;taxonomy_filter&quot;:&quot;category&quot;,&quot;additional_query_args&quot;:{&quot;tax_query&quot;:[{&quot;taxonomy&quot;:&quot;category&quot;,&quot;field&quot;:&quot;slug&quot;,&quot;terms&quot;:&quot;camera&quot;}]},&quot;content_styles&quot;:[&quot;padding-bottom: 20px&quot;],&quot;space_value&quot;:15}">
                                    <div class="qodef-grid-inner row">
                                        <div class="col-md-12 col-xs-12">
                                            <h3 class="titulos_principales" style="margin-bottom: 3%;">Nosotros</h3>
                                            <p class="descripcion_nosotros">Somos una productora artística con más de 20 años dedicada a la producción de shows y performance de gran escala, buscamos estar siempre a la vanguardia, innovando en cada una de nuestras creaciones.</p>
                                            <p class="descripcion_nosotros">Llevamos adelante estrenos de espectáculos de reconocimiento nacional e internacional, convirtiéndonos en una de las productora más importantes del interior del país.</p>
                                        </div>
                                        <div class="col-md-12 col-xs-12" id="cont-logos">
                                            <div class="qodef-grid-inner row" id="cont-logos-primary">
                                                <video poster="{{ asset('image/video/banner-video.png') }}" id="img-video" alt="Video" class="" controls playsinline >
                                                    <source src="{{ asset('image/video/pardo.mp4') }}" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-xs-12 text-center">
                                            <img src="{{ asset('image/pronto.png') }}" id="pronto" style="margin-bottom: 5%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
