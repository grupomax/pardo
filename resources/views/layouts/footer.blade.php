<section class="section_contact elementor-element elementor-element-b971b1b elementor-section-full_width qodef-elementor-content-grid qodef-in-focus elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="b971b1b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-container elementor-column-gap-no">
    </div>
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-b4c5343 elementor-column elementor-col-100 elementor-top-column" data-id="b4c5343" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-9861592 elementor-widget elementor-widget-pelicula_core_blog_list" data-id="9861592" data-element_type="widget" data-widget_type="pelicula_core_blog_list.default">
                            <div class="elementor-widget-container">
                                <div class="qodef-shortcode qodef-m  qodef-blog qodef--list qodef-skin--light qodef-item-layout--standard  qodef-grid qodef-layout--columns   qodef-gutter--normal qodef-col-num--3 qodef-item-layout--standard qodef--no-bottom-space qodef-pagination--off qodef-responsive--custom qodef-col-num--1440--3 qodef-col-num--1366--3 qodef-col-num--1024--2 qodef-col-num--768--2 qodef-col-num--680--1 qodef-col-num--480--1" data-options="{&quot;plugin&quot;:&quot;pelicula_core&quot;,&quot;module&quot;:&quot;blog\/shortcodes&quot;,&quot;shortcode&quot;:&quot;blog-list&quot;,&quot;post_type&quot;:&quot;post&quot;,&quot;next_page&quot;:&quot;2&quot;,&quot;max_pages_num&quot;:2,&quot;skin&quot;:&quot;light&quot;,&quot;behavior&quot;:&quot;columns&quot;,&quot;custom_proportions&quot;:&quot;no&quot;,&quot;images_proportion&quot;:&quot;full&quot;,&quot;columns&quot;:&quot;3&quot;,&quot;columns_responsive&quot;:&quot;custom&quot;,&quot;columns_1440&quot;:&quot;3&quot;,&quot;columns_1366&quot;:&quot;3&quot;,&quot;columns_1024&quot;:&quot;2&quot;,&quot;columns_768&quot;:&quot;2&quot;,&quot;columns_680&quot;:&quot;1&quot;,&quot;columns_480&quot;:&quot;1&quot;,&quot;space&quot;:&quot;normal&quot;,&quot;posts_per_page&quot;:&quot;3&quot;,&quot;orderby&quot;:&quot;date&quot;,&quot;order&quot;:&quot;DESC&quot;,&quot;additional_params&quot;:&quot;tax&quot;,&quot;tax&quot;:&quot;category&quot;,&quot;tax_slug&quot;:&quot;camera&quot;,&quot;layout&quot;:&quot;standard&quot;,&quot;title_tag&quot;:&quot;h4&quot;,&quot;title_custom_style&quot;:&quot;no&quot;,&quot;content_padding_bottom&quot;:&quot;20px&quot;,&quot;pagination_type&quot;:&quot;no-pagination&quot;,&quot;enable_comments&quot;:&quot;no&quot;,&quot;enable_social_share&quot;:&quot;yes&quot;,&quot;object_class_name&quot;:&quot;PeliculaCoreBlogListShortcode&quot;,&quot;taxonomy_filter&quot;:&quot;category&quot;,&quot;additional_query_args&quot;:{&quot;tax_query&quot;:[{&quot;taxonomy&quot;:&quot;category&quot;,&quot;field&quot;:&quot;slug&quot;,&quot;terms&quot;:&quot;camera&quot;}]},&quot;content_styles&quot;:[&quot;padding-bottom: 20px&quot;],&quot;space_value&quot;:15}">
                                    <div class="qodef-grid-inner row">
                                        <div class="col-md-7 col-lg-7 col-xs-12" id="formulario">
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <h3 class="titulos_principales">Contacto</h3>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    <label class="titulo-form">Nombre</label>
                                                    <input type="text" class="inputs" id="nombre-form" spellcheck="false">
                                                    <span class="nuevo-error" id="nombre_error"></span>
                                                    <br>

                                                    <label class="titulo-form">Teléfono</label>
                                                    <input type="number" class="inputs" id="telefono-form" min="0" size="10" minlength="10" maxlength="10" spellcheck="false">
                                                    <span class="nuevo-error" id="telefono_error"></span>
                                                    <br>

                                                    <label class="titulo-form">E-mail</label>
                                                    <input type="email" class="inputs" id="email-form" spellcheck="false">
                                                    <span class="nuevo-error" id="email_error"></span>
                                                </div>
                                                <div class="col-md-1">
                                                </div>
                                                <div class="col-md-5">
                                                    <label class="titulo-form">Mensaje</label>
                                                    <textarea id="mensaje-form" spellcheck="false"></textarea>
                                                    <span class="nuevo-error" id="mensaje_error"></span>
                                                </div>
                                                <div class="col-md-1">
                                                </div>
                                                <div class="col-md-4 col-xs-3 cont-boton">
                                                    <a class="items" id="boton-submit"><span class="qodef-menu-item-text">ENVIAR</span></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-lg-5 col-xs-12" id="cont-logos">
                                            <div class="qodef-grid-inner row" id="cont-logos-primary">
                                                <div class="col-md-12">
                                                    <iframe class="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6809.532583747394!2d-64.50100260745609!3d-31.420564545361067!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x942d6641264a9199%3A0xb9d838c88c6aac4a!2sHoliday%20Cinema!5e0!3m2!1ses!2sar!4v1595273404873!5m2!1ses!2sar" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                                </div>
                                                <div class="col-md-12">
                                                    <img class="icono_footer" src="{{ asset('image/iconos/ubi.png') }}">
                                                    <br>
                                                    <b id="bold_titulo">Pardo Producciones</b>
                                                    <br>
                                                    <a href="#" class="link_footer" target="_blank">9 de Julio 53, Villa Carlos Paz, Córdoba </a>
                                                </div>
                                                <div class="col-md-12">
                                                    <img class="icono_footer" src="{{ asset('image/iconos/tel.png') }}">
                                                    <br>
                                                    <a href="tel:+5493541523028" class="link_footer" target="_blank">3541523028</a>
                                                </div>
                                                <div class="col-md-12">
                                                    <img class="icono_footer" src="{{ asset('image/iconos/mail.png') }}">
                                                    <br>
                                                    <a href="mailto:pardoproducciones.arg@gmail.com" class="link_footer" target="_blank">pardoproducciones.arg@gmail.com</a>
                                                    <a href="https://www.instagram.com/pardoproductora/" target="_blank"><img class="icono_redes_footer solo_pc" src="{{ asset('image/iconos/instagram-03.svg') }}"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <p class="copirigh">Todos los derechos reservados - Pardo Producciones 2020</p>
    <p class="text-center" style="width: 100%;">
        <img src="{{ asset('image/firma.png') }}"  id="firma" alt="Christian Rouanet">
    </p>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>
<script>

    $(document).ready(function($) {
        var Body = $('body');
        Body.addClass('preloader-site');
    });

    $(window).on('load', function() {
        $('.preloader-wrapper').fadeOut();
        $('body').removeClass('preloader-site');
    });


</script>
<script>
    $('#cabecera').owlCarousel({
        loop:false,
        margin:0,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
    function openNav() {
        document.getElementById("mySidebar").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidebar").style.width = "0";
    }

    //carousel de eventos
    $('#eventos_seccion').owlCarousel({
        navigation : true,
        slideSpeed : 800,
        paginationSpeed : 800,
        singleItem: true,
        pagination: true,
        rewindSpeed: 800,
        loop:false,
        margin:45,
        nav:true,
        dots:true,
        navText: ["<img class='indicadores' src='{{ asset('image/iconos/indicador_izq.png') }}'>","<img class='indicadores' src='{{ asset('image/iconos/indicador_der.png') }}'>"],
        autoplay:false,
        autoplayTimeout:5000,
        autoplaySpeed:1500,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

    // carousel de nosotros
    $('#nosotros_seccion_caro').owlCarousel({
        navigation : true,
        slideSpeed : 800,
        paginationSpeed : 800,
        singleItem: true,
        pagination: true,
        rewindSpeed: 800,
        loop:false,
        margin:45,
        nav:true,
        dots:false,
        navText: ["<img class='indicadores' src='{{ asset('image/iconos/indicador_izq.png') }}'>","<img class='indicadores' src='{{ asset('image/iconos/indicador_der.png') }}'>"],
        autoplay:false,
        autoplayTimeout:5000,
        autoplaySpeed:1500,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });

    //formulario de contacto

    $('#boton-submit').on('click',function () {
        var nombre = $('#nombre-form').val();
        var telefono = $('#telefono-form').val();
        var email = $('#email-form').val();
        var mensaje = $('#mensaje-form').val();
        $('.nuevo-error').empty();
        valid = true;
        if(!nombre.length){
            $('#nombre_error').html('* Campo requerido');
            valid = false;
        }
        if(!telefono.length){
            $('#telefono_error').html('* Campo requerido');
            valid = false;
        }else if (telefono.length<10 || telefono.length>10){
            $('#telefono_error').html('* Debes ingresar un Teléfono valido');
            valid = false;
        }
        if(!email.length){
            $('#email_error').html('* Campo requerido');
            valid = false;
        }else if (IsEmail(email)==false){
            $('#email_error').html('* Debes ingresar un Email valido');
            valid = false;
        }
        if(!mensaje.length){
            $('#mensaje_error').html('* Campo requerido');
            valid = false;
        }
        if(valid){
            var objecte = {
                "_token":$('meta[name="csrf-token"]').attr('content'),
                "nombre":nombre,
                "telefono":telefono,
                "email":email,
                "mensaje":mensaje,
            };
            $.ajax({
                url: '/send_mail',
                method: 'POST',
                data:objecte,
                beforeSend: function(){
                    $('#boton-submit').prop('disabled', true);
                    $('#boton-submit').css('background', '#424242')
                    $('#boton-submit span').text('Enviando...');
                },
                success:function(res){
                    $('#boton-submit span').text('Enviado!');
                }
            });
        }
    });

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        } else {
            return true;
        }
    }
    $(document).ready(function() {
            //inicio
            $('.btn_posicionar').click(function () {
                closeNav();
                var target = $(this).data('donde');
                if ($(window).width() <= 930 ) {
                    $("#"+target)[0].scrollIntoView({behavior: "smooth", inline: "nearest"});
                    if(target=="formulario"){
                        setTimeout(function(){ $('#nombre-form').focus(); }, 1100);
                    }
                }else{
                    $('body').animate({
                        scrollTop: $("#"+target).offset().top
                    }, 1500);
                    if(target=="formulario"){
                        $('#nombre-form').focus();
                    }
                }
            });
    });

    //carousel de empresas
    /*$('#teatros_seccion').owlCarousel({
        navigation : true,
        slideSpeed : 800,
        paginationSpeed : 800,
        singleItem: true,
        pagination: false,
        rewindSpeed: 800,
        loop:false,
        margin:10,
        nav:true,
        navText: ["<img class='indicadores' src='{{ asset('image/iconos/indicador_izq.png') }}'>","<img class='indicadores' src='{{ asset('image/iconos/indicador_der.png') }}'>"],
        autoplay:false,
        autoplayTimeout:5000,
        autoplaySpeed:1500,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    jQuery( document ).ready(function() {
        // 1) ASSIGN EACH 'DOT' A NUMBER
        dotcount = 2;

        jQuery('.owl-dot').each(function() {
            jQuery( this ).addClass( 'dotnumber' + dotcount);
            jQuery( this ).attr('data-info', dotcount);
            dotcount=dotcount+1;
        });

        // 2) ASSIGN EACH 'SLIDE' A NUMBER
        slidecount = 1;

        jQuery('.owl-item').not('.cloned').each(function() {
            jQuery( this ).addClass( 'slidenumber' + slidecount);
            slidecount=slidecount+1;
        });

        // SYNC THE SLIDE NUMBER IMG TO ITS DOT COUNTERPART (E.G SLIDE 1 IMG TO DOT 1 BACKGROUND-IMAGE)
        jQuery('.owl-dot').each(function() {

            grab = jQuery(this).data('info');

            slidegrab = jQuery('.slidenumber'+ grab +' img').attr('data-src');
            // console.log(slidegrab);

            jQuery(this).css("background-image", "url("+slidegrab+")");

        });

        // THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
        // TO MAKE IT ALL NEAT
        amount = jQuery('.owl-dot').length;
        gotowidth = 100/amount;

        jQuery('.owl-dot').css("width", gotowidth+"%");
        newwidth = jQuery('.owl-dot').width();
        /!*jQuery('.owl-dot').css("height", newwidth+"px");*!/
        jQuery('.owl-dot').css("height", newwidth+"px");

        detect_thumbnail();
    });

    function detect_thumbnail(){
        setInterval(function(){
            $('.owl-dot').each(function(){
                if($(this).hasClass('active')){
                    var position= $(this).data('info');
                    switch (position) {
                        case 1:
                            $('.desc-desc').removeClass('active');
                            $('.desc-line-2').removeClass('active');
                            $('#desc-uno').addClass('active');
                            $('#desc-line-uno').addClass('active');
                            break;
                        case 2:
                            $('.desc-desc').removeClass('active');
                            $('.desc-line-2').removeClass('active');
                            $('#desc-dos').addClass('active');
                            $('#desc-line-dos').addClass('active');
                            break;
                        case 3:
                            $('.desc-desc').removeClass('active');
                            $('.desc-line-2').removeClass('active');
                            $('#desc-tres').addClass('active');
                            $('#desc-line-tres').addClass('active');
                            break;
                        case 4:
                            $('.desc-desc').removeClass('active');
                            $('.desc-line-2').removeClass('active');
                            $('#desc-cuatro').addClass('active');
                            $('#desc-line-cuatro').addClass('active');

                            break;
                        case 5:
                            $('.desc-desc').removeClass('active');
                            $('.desc-line-2').removeClass('active');
                            $('#desc-cinco').addClass('active');
                            $('#desc-line-cinco').addClass('active');
                            break;
                        default:
                            $('.desc-desc').removeClass('active');
                            $('.desc-line-2').removeClass('active');
                            $('#desc-uno').addClass('active');
                            $('#desc-line-uno').addClass('active');
                            break;
                    }
                }
            });
        },900);
    }
    */
</script>
