<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('image/logos/logo-ejemplo.png') }}">

    <title>Pardo Producciones</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/alertify.min.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/alertify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/default.min.css') }}" rel="stylesheet">

    <style>
       /*
       body{
          background: url("{{asset('image/butaca.jpg')}}");
          background-size: cover;
          background-position: top;
        }
        */

        .bg-black{
            background: #000000;
        }
        .logo{
            width: 65%;
        }

        .navbar-light .navbar-nav .nav-link{
            color: #ffffff !important;
            font-family: "Bebas Neue",sans-serif;
            font-weight: 400;
            text-transform: uppercase;
            font-size: 22px;
            letter-spacing: .2em;
        }
        .navbar-light .navbar-nav .nav-link:hover,.navbar-light .navbar-nav .nav-link:focus, .navbar-light .navbar-nav .nav-link:active, .navbar-light .navbar-nav .nav-link.active {
            color: #ffffff;
        }

        .dropdown-item{
            color: #ffffff;
        }

        .dropdown-menu {
            color: #ffffff;
            background-color: #000;
        }
        .ingreso{
            margin-top: 2%;
        }
        /*----------------------sidebar-menu-------------------------*/

        #sidebar-wrapper .sidebar-menu {
            padding-bottom: 10px;
        }

        #sidebar-wrapper .sidebar-menu .header-menu span {
            font-weight: bold;
            font-size: 14px;
            padding: 15px 20px 5px 20px;
            display: inline-block;
        }

        #sidebar-wrapper .sidebar-menu ul li a {
            display: inline-block;
            width: 100%;
            text-decoration: none;
            position: relative;
            padding: 8px 30px 8px 20px;
        }

        #sidebar-wrapper .sidebar-menu ul li a i {
            margin-right: 10px;
            font-size: 12px;
            width: 30px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            border-radius: 4px;
        }

        #sidebar-wrapper .sidebar-menu ul li a:hover > i::before {
            display: inline-block;
            animation: swing ease-in-out 0.5s 1 alternate;
        }

        #sidebar-wrapper .sidebar-menu .sidebar-dropdown > a:after {
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            content: "\f105";
            font-style: normal;
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;
            background: 0 0;
            position: absolute;
            right: 15px;
            top: 14px;
        }

        #sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu ul {
            padding: 5px 0;
        }

        #sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li {
            padding-left: 25px;
            font-size: 13px;
        }

        #sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li a:before {
            content: "\f111";
            font-family: "Font Awesome 5 Free";
            font-weight: 400;
            font-style: normal;
            display: inline-block;
            text-align: center;
            text-decoration: none;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            margin-right: 10px;
            font-size: 8px;
        }

        #sidebar-wrapper .sidebar-menu ul li a span.label,
        #sidebar-wrapper .sidebar-menu ul li a span.badge {
            float: right;
            margin-top: 8px;
            margin-left: 5px;
        }

        #sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li a .badge,
        #sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li a .label {
            float: right;
            margin-top: 0px;
        }

        #sidebar-wrapper .sidebar-menu .sidebar-submenu {
            display: none;
        }

        #sidebar-wrapper .sidebar-menu .sidebar-dropdown.active > a:after {
            transform: rotate(90deg);
            right: 17px;
        }

        /*--------------------------side-footer------------------------------*/
        #sidebar-wrapper li{
            color: #ffffff;
        }
        #wrapper {
            padding-left: 0;
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
        }

        #wrapper.toggled {
            padding-left: 250px;
        }

        #sidebar-wrapper {
            z-index: 1000;
            position: fixed;
            left: 250px;
            width: 0;
            height: 100%;
            margin-left: -250px;
            overflow-y: auto;
            background: #000;
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
        }

        #wrapper.toggled #sidebar-wrapper {
            width: 250px;
        }

        #page-content-wrapper {
            width: 100%;
            position: absolute;
            padding: 15px;
        }

        #wrapper.toggled #page-content-wrapper {
            position: absolute;
            margin-right: -250px;
        }

        /* Sidebar Styles */

        .sidebar-nav {
            position: absolute;
            top: 0;
            width: 250px;
            margin: 15px 5px;
            padding: 0;
            list-style: none;
        }

        .sidebar-nav li {
            text-indent: 20px;
            line-height: 40px;
        }

        .sidebar-nav li a {
            display: block;
            text-decoration: none;
            color: #ffffff;
        }

        .sidebar-nav li a:hover {
            text-decoration: none;
            color: #fff;
            background: rgba(255,255,255,0.2);
        }

        .sidebar-nav li a:active,
        .sidebar-nav li a:focus {
            text-decoration: none;
        }

        .sidebar-nav > .sidebar-brand {
            height: 65px;
            font-size: 18px;
            line-height: 60px;
        }

        .sidebar-nav > .sidebar-brand a {
            color: #999999;
        }

        .sidebar-nav > .sidebar-brand a:hover {
            color: #fff;
            background: none;
        }

        @media(min-width:768px) {
            #wrapper {
                padding-left: 250px;
            }

            #wrapper.toggled {
                padding-left: 0;
            }

            #sidebar-wrapper {
                width: 250px;
            }

            #wrapper.toggled #sidebar-wrapper {
                width: 0;
            }

            #page-content-wrapper {
                padding: 20px;
                position: relative;
            }

            #wrapper.toggled #page-content-wrapper {
                position: relative;
                margin-right: 0;
            }
            .solo_pc{
                display: block;
            }
            .solo_cel{
                display:none;
            }
        }
       @media screen and (max-width: 767px) {
           .solo_pc{
               display: none;
           }
           .solo_cel{
               display:block;
           }
           .dropdown-item {
               color: #ffffff;
               font-size: 15px;
               margin: 3% 0;
           }
           th, td{
               font-size: 12px;
           }
           .navbar-light .navbar-toggler {
               color: rgb(255 255 255) !important;
               border-color: rgb(234 234 234) !important;
           outline: none;
           }
           .navbar-light .navbar-toggler-icon {
               background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'%3e%3cpath stroke='rgba%280, 0, 0, 0.5%29' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e);
           }
       }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-black shadow-sm">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="logo" src="{{ asset('image/logo_chico.png') }}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item" style="display: none;">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Ingreso') }}</a>
                            </li>
                            @if (Route::has('register'))
                            @endif
                        @else
                            <li class="nav-item dropdown solo_pc">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('admin') }}">
                                        Panel
                                    </a>
                                    <a class="dropdown-item" href="{{ route('principal') }}">
                                        Web
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item solo_cel">
                                <a class="dropdown-item" href="{{ route('admin') }}">
                                    Panel
                                </a>
                                <a class="dropdown-item" href="{{ route('principal') }}">
                                    Web
                                </a>
                            @can('users-control')
                                    <a href="#" class="dropdown-item" >
                                        <i class="fas fa-users-cog"></i> Usuarios
                                    </a>
                                    <div class="sidebar-submenu">
                                        <ul>
                                            <li>
                                                <a href="{{ route('nuevo_usuario') }}" class="dropdown-item" >Registrar nuevo
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('ver_usuario') }}" class="dropdown-item" >Ver existentes</a>
                                            </li>
                                        </ul>
                                    </div>
                            @endcan
                                <a href="#" class="dropdown-item" >
                                    <i class="far fa-calendar-alt"></i> Proximos eventos
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('nuevo_evento') }}" class="dropdown-item" >Registrar nuevo
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('ver_evento') }}" class="dropdown-item" >Ver existentes</a>
                                        </li>
                                    </ul>
                                </div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar sesión') }}
                                </a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @guest
        @else
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                @can('users-control')
                <li class="sidebar-dropdown">
                    <a href="#">
                        <i class="fas fa-users-cog"></i> Usuarios
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a href="{{ route('nuevo_usuario') }}">Registrar nuevo
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('ver_usuario') }}">Ver existentes</a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endcan
                <li class="sidebar-dropdown">
                    <a href="#">
                        <i class="far fa-calendar-alt"></i> Proximos eventos
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a href="{{ route('nuevo_evento') }}">Registrar nuevo
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('ver_evento') }}">Ver existentes</a>
                            </li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        @endguest

        <main class="py-4 ingreso">
            @yield('content')
        </main>
    </div>
</body>
</html>
