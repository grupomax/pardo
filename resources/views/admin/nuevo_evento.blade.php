@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Registrar Evento') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            @section('content')
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            <div class="card">
                                                <div class="card-header">Registrar Evento</div>

                                                <div class="card-body">
                                                    <form method="POST" id="form-edit" role="form" enctype="multipart/form-data" action="{{ route('guardar_evento') }}">
                                                        @csrf
                                                        <div class="form-group row">
                                                            <label for="name" class="col-md-4 col-form-label text-md-right">Título *</label>
                                                            <div class="col-md-6">
                                                                <input id="titulo" type="text" placeholder="Título de la obra/evento" class="form-control" name="titulo" required value="" style="text-transform: uppercase;"  autofocus>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="email" class="col-md-4 col-form-label text-md-right">Imagen *</label>
                                                            <div class="col-md-6">
                                                                <input id="imagen" accept="image/*" type="file" class="" name="imagen" value="" required>
                                                                <img id="imagen_previo" src="" width="50%" style=" margin-right:auto; margin-left: auto; margin-top:3%;display: none;" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="email" class="col-md-4 col-form-label text-md-right">Fecha del Evento *</label>
                                                            <div class="col-md-6">
                                                                <input id="fecha_evento" type="date" class="form-control" min="<?php echo date('Y-m-d')?>" name="fecha_evento" value="<?php echo date('Y-m-d')?>" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="password" class="col-md-4 col-form-label text-md-right">Lugar *</label>
                                                            <div class="col-md-6">
                                                                <input id="lugar" type="text" placeholder="Lugar del evento" class="form-control" name="lugar" value="" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Director</label>
                                                            <div class="col-md-6">
                                                                <input id="director" type="text" class="form-control" name="director">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Link del Evento</label>
                                                            <div class="col-md-6">
                                                                <input id="link" placeholder="Link de la pagina del evento" type="text" class="form-control" name="link">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-0">
                                                            <div class="col-md-12 offset-md-4">
                                                                <button type="submit" class="btn btn-primary">
                                                                    Registrar
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endsection
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script>
    $( document ).ready(function() {
        $('#imagen').change(function () {
            $('#imagen_previo').attr('src','');
            $('#imagen_previo').css('display','block');
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagen_previo').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
});

</script>
