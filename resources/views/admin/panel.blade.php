@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Panel') }}</div>
                <div class="card-body">
                    <div class="alert alert-primary" role="alert">
                       <h5> Hola {{ Auth::user()->name }}!</h5>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
