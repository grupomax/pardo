@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Editar Evento') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @section('content')
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-md-10">
                                        <div class="card">
                                            <div class="card-header">Editar Evento</div>

                                            <div class="card-body">
                                                <form method="POST" id="form-edit" role="form" enctype="multipart/form-data" action="{{ route('edit_evento') }}">
                                                    @csrf
                                                    <input id="id_evento" type="hidden" class="form-control" name="id_evento" required value="{{$evento->id}}">
                                                    <input id="old_imagen" type="hidden" class="form-control" name="old_imagen" required value="{{$evento->imagen}}">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-md-4 col-form-label text-md-right">Título *</label>
                                                        <div class="col-md-6">
                                                            <input id="titulo" type="text" placeholder="Título de la obra/evento" class="form-control" name="titulo" required value="{{$evento->titulo}}" style="text-transform: uppercase;" autocomplete="false" autofocus>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="email" class="col-md-4 col-form-label text-md-right">Imagen</label>
                                                        <div class="col-md-6">
                                                            <input id="imagen" accept="image/*" type="file" class="" name="imagen" value="">
                                                            <div class="row" style="margin-top: 3%; margin-bottom: 1%;">
                                                                <div class="col-md-6">
                                                                    <span class="badge badge-pill badge-secondary">Anterior</span>
                                                                    <img id="imagen_anterior" src="{{ asset('image/publicados/'.$evento->imagen) }}" width="95%" style=" margin-right:auto; margin-left: auto; margin-top:3%;" alt="">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <span class="badge badge-pill badge-primary">Nuevo</span>
                                                                    <img id="imagen_nuevo" src="" width="95%" style=" margin-right:auto; margin-left: auto; margin-top:3%;display: none;" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="email" class="col-md-4 col-form-label text-md-right">Fecha del Evento *</label>
                                                        <div class="col-md-6">
                                                            <input id="fecha_evento" type="date" class="form-control" min="<?php echo date('Y-m-d')?>" name="fecha_evento" value="{{ \Carbon\Carbon::parse($evento->fecha_evento)->format('Y-m-d') }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="password" class="col-md-4 col-form-label text-md-right">Lugar *</label>
                                                        <div class="col-md-6">
                                                            <input id="lugar" type="text" placeholder="Lugar del evento" class="form-control" name="lugar" value="{{$evento->lugar}}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Director</label>
                                                        <div class="col-md-6">
                                                            <input id="director" type="text" class="form-control" name="director" value="{{$evento->director}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Link del Evento</label>
                                                        <div class="col-md-6">
                                                            <input id="link" placeholder="Link de la pagina del evento" type="text" class="form-control" name="link" value="{{$evento->link}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-0">
                                                        <div class="col-md-12 offset-md-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                Editar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endsection
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script>
    $( document ).ready(function() {
        $('#imagen').change(function () {
            $('#imagen_nuevo').attr('src','');
            $('#imagen_nuevo').css('display','block');
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagen_nuevo').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    });
</script>
