@extends('layouts.app')
<style>
.acciones{
    margin: 0 7%;
}
    td a{
        cursor: pointer;
    }
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 offset-md-2">
                <div class="card">
                    <div class="card-header">{{ __('Usuarios') }}</div>
                    <div class="card-body bs-example container table-responsive" data-example-id="striped-table">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre y Apellido</th>
                                    <th>Email</th>
                                    <th>Tipo de Usuario</th>
                                    <th>Fecha de Registro</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr @if($user->status==0) style="background:#e91e637a;" class="suspen_row" @endif>
                                    <th scope="row">{{ $user->id }}</th>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>@if($user->user_type==1) Administrador @else Colaborador @endif</td>
                                    <td>{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y h:i') }}</td>
                                    <td>
                                        <a href="/editar/{{$user->id}}"><i class="far fa-edit acciones"></i></a>
                                        @if($user->status==1)
                                            <a onclick="deshabilitar({{$user->id}});"><i class="far fa-minus-square acciones" style="color: #ffc107;"></i></a>
                                        @else
                                            <a onclick="habilitar({{$user->id}});"><i class="far fa-plus-square acciones"  style="color: #009688;"></i></a>
                                        @endif
                                        <a onclick="borrar({{$user->id}});"><i class="far fa-trash-alt acciones" style="color: red;"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    function borrar(id) {
        alertify.confirm('Eliminar usuario','¿Estas seguro de eliminar este usuario?',function () {
            window.location = '/delete/'+id;
            alertify.alert().set({
                'message': '<p class="text-center"><i class="fas fa-sync fa-spin" style="font-size: 48px;"></i></p>' ,
                'basic': true,
                'closable':false,
                'movable': false
            }).show();
        },function () {
            alertify.error('acción cancelada');
        }).set('labels',{ok:'Si',cancel:'No'});
    }

    function habilitar(id) {
        alertify.confirm('Habilitar usuario','¿Estas seguro de habilitar el acceso al sistema a este usuario?',function () {
            window.location = '/habilitar/'+id;
            alertify.alert().set({
                'message': '<p class="text-center"><i class="fas fa-sync fa-spin" style="font-size: 48px;"></i></p>' ,
                'basic': true,
                'closable':false,
                'movable': false
            }).show();
        },function () {
            alertify.error('acción cancelada');
        }).set('labels',{ok:'Si',cancel:'No'});
    }

    function deshabilitar(id) {
        alertify.confirm('Deshabilitar usuario','¿Estas seguro de deshabilitar el acceso al sistema a este usuario?',function () {
            window.location = '/deshabilitar/'+id;
            alertify.alert().set({
                'message': '<p class="text-center"><i class="fas fa-sync fa-spin" style="font-size: 48px;"></i></p>' ,
                'basic': true,
                'closable':false,
                'movable': false
            }).show();
        },function () {
            alertify.error('acción cancelada');
        }).set('labels',{ok:'Si',cancel:'No'});
    }

    $(document).ready(function() {
        $('.suspen_row').popover({
            container: 'body',
            toggle: 'popover',
            placement: 'top',
            content: 'Este usuario tiene restringido el acceso al sistema',
            trigger: 'hover'
        });

    @if (\Session::has('repuesta'))
        alertify.success(`{{\Session::get('repuesta')}}`);
    @endif
    });
</script>
