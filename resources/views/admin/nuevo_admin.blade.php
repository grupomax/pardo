@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('auth.register')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('#password').keypress(function(e) {
            $('#mayuscula').remove();
            var s = String.fromCharCode( e.which );
            if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
                //alert('Bloq Mayus está activado.');
                $('.card-header').after(`<span id="mayuscula" class="alert alert-warning alert-dismissible fade show">Mayuscula activada</span>`);
            }
        });
        $('#password-confirm').keypress(function(e) {
            $('#mayuscula').remove();
            var s = String.fromCharCode( e.which );
            if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
                //alert('Bloq Mayus está activado.');
                $('.card-header').after(`<span id="mayuscula" class="alert alert-warning alert-dismissible fade show">Mayuscula activada</span>`);
            }
        });
    });
</script>
