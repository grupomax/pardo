@extends('layouts.app')
<style>
.acciones{
    margin: 0 7%;
}
    td a{
        cursor: pointer;
    }
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 offset-md-2">
                <div class="card">
                    <div class="card-header">{{ __('Eventos') }}</div>
                    <div class="card-body bs-example container table-responsive" data-example-id="striped-table">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Título</th>
                                    <th>Imagen</th>
                                    <th>Lugar</th>
                                    <th>Fecha del evento</th>
                                    <th>Publicador</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($eventos as $evento)
                                <tr @if($evento->estado==0) style="background:#e91e637a;" class="suspen_row" @endif>
                                    <th scope="row">{{ $evento->id }}</th>
                                    <td>{{ $evento->titulo }}</td>
                                    <td><img width="45px" height="45px" src="{{ asset('image/publicados/'.$evento->imagen) }}"></td>
                                    <td>{{ $evento->lugar }}</td>
                                    <td>{{ \Carbon\Carbon::parse($evento->fecha_evento)->format('d/m/Y') }}</td>
                                    <td>{{ $evento->name }}</td>
                                    <td>
                                        <a href="/editar_evento/{{$evento->id}}"><i class="far fa-edit acciones"></i></a>
                                        @if($evento->estado==1)
                                            <a onclick="deshabilitar({{$evento->id}});"><i class="far fa-minus-square acciones" style="color: #ffc107;"></i></a>
                                        @else
                                            <a onclick="habilitar({{$evento->id}});"><i class="far fa-plus-square acciones"  style="color: #009688;"></i></a>
                                        @endif
                                        <a onclick="borrar({{$evento->id}});"><i class="far fa-trash-alt acciones" style="color: red;"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        {{ $eventos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    function borrar(id) {
        alertify.confirm('Eliminar evento','¿Estas seguro de eliminar este evento?',function () {
            window.location = '/delete_event/'+id;
            alertify.alert().set({
                'message': '<p class="text-center"><i class="fas fa-sync fa-spin" style="font-size: 48px;"></i></p>' ,
                'basic': true,
                'closable':false,
                'movable': false
            }).show();
        },function () {
            alertify.error('acción cancelada');
        }).set('labels',{ok:'Si',cancel:'No'});
    }

    function habilitar(id) {
        alertify.confirm('Activar evento','¿Estas seguro de activar este evento?',function () {
            window.location = '/habilitar_event/'+id;
            alertify.alert().set({
                'message': '<p class="text-center"><i class="fas fa-sync fa-spin" style="font-size: 48px;"></i></p>' ,
                'basic': true,
                'closable':false,
                'movable': false
            }).show();
        },function () {
            alertify.error('acción cancelada');
        }).set('labels',{ok:'Si',cancel:'No'});
    }

    function deshabilitar(id) {
        alertify.confirm('Dar de baja evento','¿Estas seguro de dar de baja este evento?',function () {
            window.location = '/deshabilitar_event/'+id;
            alertify.alert().set({
                'message': '<p class="text-center"><i class="fas fa-sync fa-spin" style="font-size: 48px;"></i></p>' ,
                'basic': true,
                'closable':false,
                'movable': false
            }).show();
        },function () {
            alertify.error('acción cancelada');
        }).set('labels',{ok:'Si',cancel:'No'});
    }

    $(document).ready(function() {
        $('.suspen_row').popover({
            container: 'body',
            toggle: 'popover',
            placement: 'top',
            content: 'Este evento esta dado de baja',
            trigger: 'hover'
        });

    @if (\Session::has('repuesta'))
        alertify.success(`{{\Session::get('repuesta')}}`);
    @endif

    });

</script>
