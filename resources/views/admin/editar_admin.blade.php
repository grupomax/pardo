@extends('layouts.app')
<style>
#mal_mail{
    padding: 0 2%;
}
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Editar Usuario') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            @section('content')
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            <div class="card">
                                                <div class="card-header">Editar Usuario</div>

                                                <div class="card-body">
                                                    <form method="POST" id="form-edit" role="form" action="{{ route('edit') }}">
                                                        @csrf
                                                        <input type="hidden" id="id_user" name="id_user" value="{{$usuario->id}}">
                                                        <div class="form-group row">
                                                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombre y Apellido</label>
                                                            <div class="col-md-6">
                                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $usuario->name }}" required autocomplete="name" autofocus>
                                                                @error('name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                 </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="email" class="col-md-4 col-form-label text-md-right">Correo Electrónico</label>
                                                            <div class="col-md-6">
                                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $usuario->email }}" required autocomplete="email">
                                                                @error('email')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="email" class="col-md-4 col-form-label text-md-right">Tipo de Usuario</label>
                                                            <div class="col-md-6">
                                                                <select id="user_type" name="user_type" class="form-control" required autocomplete="user_type">
                                                                    <option value="1" @if($usuario->user_type==1) selected @endif>Administrador</option>
                                                                    <option value="2" @if($usuario->user_type==2) selected @endif>Colaborador</option>
                                                                </select>
                                                                @error('user_type')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>
                                                            <div class="col-md-6">
                                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                                                                @error('password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Repetir Contraseña</label>

                                                            <div class="col-md-6">
                                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-0">
                                                            <div class="col-md-12 offset-md-4">
                                                                <button type="submit" id="update" class="btn btn-primary">
                                                                    Actualizar
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endsection
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        $('#password').keypress(function(e) {
            $('#mayuscula').remove();
            var s = String.fromCharCode( e.which );
            if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
                //alert('Bloq Mayus está activado.');
                $('.card-header').after(`<span id="mayuscula" class="alert alert-warning alert-dismissible fade show">Mayuscula activada</span>`);
            }
        });
        $('#password-confirm').keypress(function(e) {
            $('#mayuscula').remove();
            var s = String.fromCharCode( e.which );
            if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
                //alert('Bloq Mayus está activado.');
                $('.card-header').after(`<span id="mayuscula" class="alert alert-warning alert-dismissible fade show">Mayuscula activada</span>`);
            }
        });


        $('#email').change(function(e) {
            verificar_mail();
        });


        $('#password').change(function(e) {
            verificar_pass();
        });
        $('#password-confirm').change(function(e) {
            verificar_pass();
        });
            //valdiamos campos del formulario antes de enviarlo
        $("#form-edit").submit(function(e){
            e.preventDefault();
            valid = true;
            verificar_mail();
            verificar_pass();
            if(valid){
                $('#update').prop("disabled", true);
                $('#update').text("Actualizando...");
                e.currentTarget.submit();

            }
        });
    });

    function verificar_mail() {
        $('#mal_mail').remove();
        var data = {
            _token: $('meta[name=csrf-token]').attr('content'),
            id : $('#id_user').val(),
            email: $('#email').val()
        };
        $.ajax({
            url: '/get_mail',
            method: 'POST',
            data:data,
            success:function(res){
                if(res.status==true){
                    $('#update').prop("disabled", true);
                    $('#mal_mail').remove();
                    $('#email').after(`<span id="mal_mail" class="alert-danger">El Correo Electrónico esta siendo utilizado por otro usuario</span>`);
                    valid = false;
                }else{
                    $('#update').prop("disabled", false);
                }
            }
        });
    }

    function verificar_pass() {
        $('#mal_pass').remove();
        $('#mal_pass_conf').remove();

        var pasword = $('#password').val();
        var pasword_conf = $('#password-confirm').val();

        if(pasword && pasword.length<8){
            $('#update').prop("disabled", true);
            $('#mal_pass').remove();
            $('#password').after(`<span id="mal_pass" class="alert-danger">Ingresa una Contraseña mayor o igual a 8 caracteres</span>`);
            valid = false;
        }else{
            $('#update').prop("disabled", false);
        }

        if(pasword && pasword.length>=8 && !pasword_conf){
            $('#update').prop("disabled", true);
            $('#mal_pass_conf').remove();
            $('#password-confirm').after(`<span id="mal_pass_conf" class="alert-danger">Debes repetir la Contraseña</span>`);
            valid = false;
        }else{
            $('#update').prop("disabled", false);
        }

        if(pasword_conf && pasword_conf.length<8){
            $('#update').prop("disabled", true);
            $('#mal_pass_conf').remove();
            $('#password-confirm').after(`<span id="mal_pass_conf" class="alert-danger">Ingresa una Contraseña mayor o igual a 8 caracteres</span>`);
            valid = false;
        }else{
            $('#update').prop("disabled", false);
        }

        if(pasword_conf && pasword_conf.length>=8 && !pasword){
            $('#update').prop("disabled", true);
            $('#mal_pass').remove();
            $('#password').after(`<span id="mal_pass" class="alert-danger">Ingresa una Contraseña contraseña</span>`);
            valid = false;
        }else{
            $('#update').prop("disabled", false);
        }

        if( pasword.length>=8 && pasword_conf.length>=8 && pasword!=pasword_conf){
            $('#update').prop("disabled", true);
            $('#mal_pass_conf').remove();
            $('#password-confirm').after(`<span id="mal_pass_conf" class="alert-danger">Las Contraseñas no coinciden</span>`);
            valid = false;
        }else{
            $('#update').prop("disabled", false);
        }
    }
</script>


