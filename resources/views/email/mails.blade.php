<!DOCTYPE html>
<html>
<head>
    <title>Pardo Producciones</title>
</head>
<body>
<div class="container" style=" background: #f3f3f3; padding: 3%;">
    <h2>Ingreso una nueva consulta en la página de pardo producciones</h2>
    <br>
    <legend style="font-size: 17px;font-family: 'Lato'"><b>Nombre:</b> {{ $details['nombre'] }}</legend>
    <br>
    <legend  style="font-size: 17px;font-family: 'Lato'"><b>Teléfono:</b> {{ $details['telefono'] }}</legend>
    <br>
    <legend style="font-size: 17px;font-family: 'Lato'"><b>Email:</b> {{ $details['email'] }}</legend>
    <br>
    <legend  style="font-size: 17px;font-family: 'Lato'"><b>Mensaje:</b> {{ $details['mensaje'] }}</legend>
    <br>
</div>
<small>Éste correo se envió de forma automática. Por favor no respondas a este mail.</small>
</body>
</html>
